
Pod::Spec.new do |s|
  s.name             = 'WXApiManager'
  s.version          = '0.1.4'
  s.summary          = 'A short description of WXApiManager.'

  s.description      = 'WXApiManager desc Information'
  s.homepage         = 'https://gitee.com/wx_ios/WXApiManager'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yangzi' => '595919268@qq.com' }
  s.source           = { :git => 'https://gitee.com/wx_ios/WXApiManager.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'

  s.source_files = 'WXApiManager/Classes/**/*'

  s.dependency 'EncryptionTool'
  s.dependency 'XXDeviceTool'
  s.dependency 'XXNetwork'
  s.dependency 'MBHudManager'

end
